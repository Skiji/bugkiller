# Project Timekiller
Timekiller je aplikacija za deljenje idej med učitelji, animatorji, starši..



# Kako prijavim napako?
Če ste opazili napako, jo lahko nam sporočite na kar tri načine.
- Preko feedback-a (https://d2.si/f/ideja/ap/feedback)
- Preko maila: nujno@d2.si
- (NAJBOLJE) Preko Githuba (kjer ste zdaj)


# Zapisnik sprememb

### Verzija A1B1C53
- NEW: Feedback sistem

### Verzija A1B1C20
- NEW: Like/dislike sistem
- FIX: Objavlanje idej